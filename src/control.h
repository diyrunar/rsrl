/* Stuff related to controlling the player character or monsters. */

#ifndef CONTROL_H
#define CONTROL_H

#include <memory>
#include <SFML/Graphics.hpp>

extern "C" {
#include "lua.h"
#include "lauxlib.h"
}

#include "being.h"
#include "map.h"
#include "graphicssystem.h"

/* Converts a key to the correct action */
std::function<void (unsigned int)> make_action (sf::Event &event,
												BeingList &beings,
												Map &map,
												GraphicsSystem &gfx_system,
												lua_State *L,
												unsigned int player_id);

/* A functor for moving. Movement works the same regardless of direction, so
 * a functor is better than a lambda here. */
class MoveAction
{
	public:
		MoveAction(Map &map, GraphicsSystem &gfx_system, sf::Event &event);
		void operator()(unsigned int id) const;
	
	private:
		Map &_map;
		GraphicsSystem &_gfx_system;
		int _dx;
		int _dy;
};

#endif /* end of include guard: CONTROL_H */
