#ifndef BEING_H
#define BEING_H

/* A class for the player character and monsters. */

#include <string>
#include <vector>
#include <memory>
#include <map>
#include "item.h"

extern "C"
{
#include <lua.h>
#include <lauxlib.h>
}

class Being
{
	public:
		/* Constructor */
		Being (lua_State *L, std::string &type);

		/*
		 * Getters / setters
		 */
		std::string get_type () const { return _type; }
		int get_hp () const { return _hp; }
		int get_maxhp () const { return _maxhp; }

		/* Increases the Being's HP by the given amount.
		 * Can be negative to decrease HP */
		void adjust_hp (int amount);

		bool is_dead () const;

		/* Add item to inventory. Returns true if successful */
		bool add_item (std::string &type, unsigned int amount);

		/* Removes item from inventory and returns it */
		std::unique_ptr<item_data> remove_item (unsigned int index);

		/* Returns all items in inventory */
		std::unique_ptr<std::vector<item_data>> get_items () const;

		/* Returns the names of all items in inventory */
		std::unique_ptr<std::vector<std::string>> get_item_names (lua_State *L) const;

		/* Equips a weapon at the specified inventory index.
		 * Returns true if successful */
		bool equip_weapon (unsigned int index);

		/* Returns the item_data of all equipped items */
		std::unique_ptr<std::vector<item_data>> get_equipment () const;

	private:
		/* What kind of a Being is this? */
		std::string _type;

		/* Stats */
		int _hp;
		int _maxhp;

		/* Inventory */
		std::vector<item_data> _inventory;

		/* Equipment */
		item_data _weapon;
};

/* This typedef is used *a lot*. It should probably be defined somewhere else,
 * but it doesn't matter that much. */
typedef std::map<unsigned int, Being> BeingList;

#endif /* end of include guard: BEING_H */
