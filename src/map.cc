/* This class contains a single map. */

#include "map.h"
#include "util.h"
#include "exception.h"

Map::Map (std::mt19937 &rng,
          BeingList &beings,
          IDGenerator &id_gen,
          lua_State *L,
          unsigned int player_id) :
	_beings(beings),
	_id_gen(id_gen),
	_L(L)
{
	/* Ask Lua to generate a new map */
	lua_getglobal (_L, "get_map");
	lua_pcall (_L, 0, 1, 0);
	lua_getfield (_L, -1, "height");
	const int map_h = lua_tointeger (_L, -1);
	lua_pop (_L, 1);
	lua_getfield (_L, -1, "width");
	const int map_w = lua_tointeger (_L, -1);
	lua_pop (_L, 1);
	lua_getfield (_L, -1, "tiles");
	const std::string tiles (lua_tostring(_L, -1));
	lua_pop (_L, 1);

	_height = map_h;
	_width = map_w;

	for (const char &c: tiles) {
		// TODO: use something other than a switch.
		switch (c) {
			case '.':
				_tiles.push_back(Tile::FLOOR);
				break;
			case '#':
				_tiles.push_back(Tile::WALL);
				break;
			default:
				break;
		}
	}

	/* Add items */
	lua_getfield (_L, -1, "items");
	if (lua_istable(_L, -1)) {
		const int num_items = luaL_len (_L, -1);
		for (int i = 1; i <= num_items; ++i) {
			lua_rawgeti (_L, -1, i); // get the next item
			lua_getfield (_L, -1, "x");
			const int x = lua_tonumber (_L, -1);
			lua_pop (_L, 1);
			lua_getfield (_L, -1, "y");
			const int y = lua_tonumber (_L, -1);
			lua_pop (_L, 1);
			lua_getfield (_L, -1, "item");
			const std::string item_type (lua_tostring (_L, -1));
			lua_pop (_L, 1);
			if (is_open(x, y)) {
				add_item (item_type, 1, x, y);
			}
			lua_pop (_L, 1);
		}
	}
	lua_pop (_L, 2); // the whole map table + items table

	/* Add player to map */
	std::uniform_int_distribution<> d_x (0, _width - 1);
	std::uniform_int_distribution<> d_y (0, _height - 1);
	int px, py;
	do {
		px = d_x(rng);
		py = d_y(rng);
	} while (!is_open(px, py));
	_local_beings.push_back({player_id, px, py});

	/* Create a few monsters */
	for (int i = 0; i < 10; ++i) {
		/* Random coordinates */
		int x = d_x (rng);
		int y = d_y (rng);
		if (is_open(x, y)) {
			unsigned int id = _id_gen.new_id();
			std::string tmp ("test_monster");
			_beings.emplace (id, Being(L, tmp));
			_local_beings.push_back ({id, x, y});
		}
	}
}

/* Returns the character that should be drawn in a specific square */
char Map::get_symbol (int x, int y) const
{
	if (!is_in_bounds(x, y)) {
		throw OutOfBounds();
	}

	int pos = get_array_pos (x, y, _width);

	/* If an item is a tile, it should be printed. */
	if (_items.count(pos) > 0 && _items.at(pos).size() > 0) {
		std::string item = _items.at(pos).back().type;
		lua_getglobal (_L, "get_item_symbol");
		lua_pushstring (_L, item.c_str());
		lua_pcall (_L, 1, 1, 0);
		if (lua_isstring (_L, -1)) {
			std::string result (lua_tostring (_L, -1));
			return result[0]; /* Lua uses strings for characters */
		}
	}
	switch (_tiles[pos]) {
		case Tile::FLOOR:
			return '.';
			break;
		case Tile::WALL:
			return '#';
			break;
		default: /* ??? */
			return ' ';
			break;
	}
}

/* Can a being move on a specific square? */
bool Map::is_open (int x, int y) const
{
	/* Out of bounds? */
	if (!is_in_bounds(x, y)) {
		return false;
	}

	int pos = get_array_pos (x, y, _width);
	return (_tiles[pos] == Tile::FLOOR);
}

bool Map::is_in_bounds (int x, int y) const
{
	return (x >= 0 && x < _width && y >= 0 && y < _height);
}

std::vector<being_coords>& Map::get_beings ()
{
	return _local_beings;
}

being_coords Map::get_coords (unsigned int id) const
{
	for (const being_coords& c : _local_beings) {
		if (c.id == id) {
			return c;
		}
	}

	return {0, -1, -1}; // Being was not found
}

void Map::move_being (unsigned int id, int dx, int dy)
{
	for (being_coords &c : _local_beings) {
		if (c.id == id) {
			/* Check for out of bounds */
			if (!is_in_bounds(c.x + dx, c.y + dy)) {
				return;
			}

			if (!is_open(c.x + dx, c.y + dy)) {
				throw CantGoThere();
			}

			/* If there is an enemy on the target tile, attack it. */
			unsigned int enemy = get_being_on_tile (c.x + dx, c.y + dy);
			if (enemy) {
				lua_getglobal (_L, "attack");
				lua_pushnumber (_L, id);
				lua_pushnumber (_L, enemy);
				lua_pcall (_L, 2, 0, 0);
			} else {
				c.x += dx;
				c.y += dy;
			}
		}
	}
}

unsigned int Map::get_being_on_tile (int x, int y) const
{
	for (const being_coords &b: _local_beings) {
		if (b.x == x && b.y == y) {
			return b.id;
		}
	}

	return 0; // no Being on the given tile
}

void Map::remove_being (unsigned int id)
{
	auto it = _local_beings.begin();
	while (it != _local_beings.end()) {
		if (it->id == id) {
			_local_beings.erase (it);
			return;
		} else {
			it++;
		}
	}
}

void Map::add_being (unsigned int id, int x, int y)
{
	if (is_in_bounds(x, y)) {
		// TODO: check that Being with given id exists.
		_local_beings.push_back ({id, x, y});
	}
}

std::unique_ptr<std::vector<item_data>> Map::get_items_on_tile
	(int x, int y) const
{
	/* out of bounds? */
	if (!is_in_bounds(x, y)) {
		return nullptr;
	}

	std::unique_ptr<std::vector<item_data>> items (new std::vector<item_data>);
	unsigned int array_pos = get_array_pos (x, y, _width);
	for (auto &it : _items) {
		for (auto &item: it.second) {
			if (it.first == array_pos) {
				items->push_back (item);
			}
		}
	}

	return items;
}

void Map::add_item (const std::string &item, unsigned int amount, int x, int y)
{
	/* out of bounds? */
	if (!is_in_bounds(x, y)) {
		return;
	}

	unsigned int pos = get_array_pos (x, y, _width);
	_items[pos].push_back ({item, amount});
}

std::unique_ptr<item_data> Map::remove_item (int x, int y, unsigned int index)
{
	unsigned int pos = get_array_pos (x, y, _width);
	if (_items.count(pos) > 0) { // Check if there are any items on the tile
		if (_items.at(pos).size() > index) { // Check that index exists
			std::unique_ptr<item_data> tmp (new item_data);
			*tmp = _items.at(pos).at(index);
			_items.at(pos).erase (_items.at(pos).begin() + index);
			return tmp; // Return the removed item
		}
	}

	return nullptr; // Removing failed
}

std::unique_ptr<std::vector<unsigned int>> Map::get_beings_in_radius
	(int x, int y, int r)
{
	std::unique_ptr<std::vector<unsigned int>> being_list (new std::vector<unsigned int>);
	for (const being_coords &b: _local_beings) {
		/* A Being can move one square in any direction. Therefore, the steps
		 * required to move from square a to square b is either the difference
		 * between the x-coordinates or the difference between the
		 * y-coordinates; whichever is higher. */
		int dx = abs(x - b.x);
		int dy = abs(y - b.y);
		int dist = dx > dy ? dx : dy;
		if (dist <= r) {
			being_list->push_back(b.id);
		}
	}

	return being_list;
}

/* Dijkstra's algorithm. */
std::unique_ptr<std::pair<int, int>> Map::find_path
	(int x1, int y1, int x2, int y2) const
{
	/* Are the start and goal points within bounds and accessible? */
	if (!is_in_bounds(x1, y1) || !is_in_bounds(x2, y2)
	 || !is_open(x1, y1) || !is_open(x2, y2)) {
		return nullptr;
	}

	std::vector<int> distances (_width * _height, -1);
	std::vector<std::pair<int, int>> open;

	open.push_back (std::pair<int, int>(x2, y2));

	while (!open.empty()) {
		auto curr_tile = open.front();
		int  curr_pos  = get_array_pos (curr_tile.first, curr_tile.second, _width);
		auto surrounding_tiles = get_surrounding_tiles
			(curr_tile.first, curr_tile.second);
		for (int pos : *surrounding_tiles) {
			if (distances[pos] == -1) {
				distances[pos] = distances[curr_pos] + 1;
				/* Start tile? */
				if (pos == get_array_pos (x1, y1, _width)) {
					goto path_found;
				} else {
					open.push_back (std::pair<int, int>(pos % _width, pos / _width));
				}
			}
		}
		open.erase (open.begin());
	}
	return nullptr; /* no path exists */

path_found:
	/* Return the next move.
	 * Manhattan distance is used to calculate the straightest path. */
	int best_pos;
	int best_manh = 99999999;
	auto surrounding_tiles = get_surrounding_tiles (x1, y1);
	best_pos = surrounding_tiles->front();
	for (int pos : *surrounding_tiles) {
		if (distances[pos] < distances[get_array_pos(x1, y1, _width)]
		 && distances[pos] != -1) {
			int manh = manhattan(x2, y2, (pos % _width), (pos / _width));
			if (manh < best_manh) {
				best_pos = pos;
				best_manh = manh;
			}
		}
	}
	return std::unique_ptr<std::pair<int, int>>
		(new std::pair<int, int>((best_pos % _width) - x1, (best_pos / _width) - y1));

	/* ??? */
	return nullptr;
}

std::unique_ptr<std::vector<int>> Map::get_surrounding_tiles (int x, int y) const
{
	std::unique_ptr<std::vector<int>> tiles (new std::vector<int>);
	for (int i = -1; i <= 1; ++i) {
		for (int j = -1; j <= 1; ++j) {
			if (is_in_bounds(x+i, y+j) && is_open(x+i, y+j)) {
				tiles->push_back(get_array_pos(x+i, y+j, _width));
			}
		}
	}

	return tiles;
}
