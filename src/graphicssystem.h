/* This class takes care of drawing stuff on screen. */

#ifndef GRAPHICSSYSTEM_H
#define GRAPHICSSYSTEM_H

#include <array>
#include <vector>
#include <string>
#include <map>

#include <SFML/Graphics.hpp>

#include "map.h"
#include "being.h"

extern "C"
{
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

class GraphicsSystem
{
	public:
		/* Constructor */
		GraphicsSystem (sf::RenderWindow &game_win);
	
		/* Draw the map */
		void draw_map (Map &map, BeingList &beings, lua_State *L);

		/* Draw player stats */
		void draw_stats (int hp, int maxhp);

		/* Add a message to the message queue */
		void add_message (std::string &msg);

		/* Print all messages in the queue */
		void print_messages ();

		/* Display everything on screen */
		void display ();

		/* Shows the player a menu and returns the player's choice */
		int show_menu (std::string &title, std::vector<std::string> &options);

	private: /* Draw one character in the game view */
		void draw_char_in_view (int x, int y, char c);

		/* Game view size */
		static const int VIEW_W = 60;
		static const int VIEW_H = 25;

		/* Font for drawing the map */
		sf::Font _game_font;

		/* Window for drawing the graphics */
		sf::RenderWindow &_game_win;

		/* Currently drawn portion of the game map */
		std::array<sf::Text, VIEW_H*VIEW_W> _game_view;

		/* Messages shown on the screen at once */
		static const int SHOWN_MESSAGES = 10;

		/* Message queue */
		std::vector<std::string> _msg_queue;

		/* Messages currently shown on the screen */
		std::array<sf::Text, SHOWN_MESSAGES> _messages;
};

#endif /* end of include guard: GRAPHICSSYSTEM_H */
