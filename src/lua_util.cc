#include "lua_util.h"

void lua_util::add_to_registry (lua_State *L, const std::string &name, void *data)
{
	lua_util::add_to_registry (L, name.c_str(), data);
}

void lua_util::add_to_registry (lua_State *L, const char *name, void *data)
{
	lua_pushstring (L, name);
	lua_pushlightuserdata (L, data);
	lua_settable (L, LUA_REGISTRYINDEX);
}

void *lua_util::get_from_registry (lua_State *L, const std::string &name)
{
	return lua_util::get_from_registry (L, name.c_str());
}

void *lua_util::get_from_registry (lua_State *L, const char *name)
{
	lua_getfield (L, LUA_REGISTRYINDEX, name);
	if (lua_isuserdata(L, -1)) {
		void *tmp = lua_touserdata (L, -1);
		lua_pop (L, 1);
		return tmp;
	} else {
		lua_pop (L, 1);
		return nullptr;
	}
}

std::string lua_util::get_item_name (lua_State *L, const std::string &item)
{
	return lua_util::get_item_name (L, item.c_str());
}

std::string lua_util::get_item_name (lua_State *L, const char *item)
{
	lua_getglobal (L, "get_item_name");
	lua_pushstring (L, item);
	lua_pcall (L, 1, 1, 0);

	if (lua_isstring(L, -1)) {
		std::string name (lua_tostring(L, -1));
		lua_pop (L, 1);
		return name;
	} else {
		lua_pop (L, 1);
		return std::string ("null");
	}
}
