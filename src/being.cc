#include "being.h"
#include "lua_util.h"

/* Constructor */
Being::Being (lua_State *L, std::string &type):
	_type (type),
	_weapon ({"null", 0})
{
	/* Set stats */
	lua_getglobal(L, "get_mon_maxhp");
	lua_pushstring(L, _type.c_str());
	lua_pcall(L, 1, 1, 0);
	int hp = 0;
	if (lua_isnumber(L, -1)) {
		hp = lua_tointeger(L, -1);
	}
	lua_pop (L, 1);
	_hp = _maxhp = hp;
}

void Being::adjust_hp (int amount)
{
	_hp += amount;
	if (_hp > _maxhp) {
		_hp = _maxhp;
	}
}

bool Being::is_dead () const
{
	return _hp <= 0;
}

bool Being::add_item (std::string &type, unsigned int amount)
{
	/* Does the Being already have this item? In that case just
	 * increase the amount */
	// TODO: check if item is stackable.
	for (auto &item : _inventory) {
		if (item.type == type) {
			item.amount += amount;
			return true;
		}
	}

	_inventory.push_back ({type, amount});
	return true;
}

std::unique_ptr<item_data> Being::remove_item (unsigned int index)
{
	if (_inventory.size() > index) {
		std::unique_ptr<item_data> tmp (new item_data);
		*tmp = _inventory.at(index);
		_inventory.erase (_inventory.begin() + index);
		return tmp;
	} else { // index out of bounds
		return nullptr;
	}
}

std::unique_ptr<std::vector<item_data>> Being::get_items () const
{
	std::unique_ptr<std::vector<item_data>> tmp
		(new std::vector<item_data>);
	for (auto &item : _inventory) {
		tmp->push_back (item);
	}
	return tmp;
}

std::unique_ptr<std::vector<std::string>> Being::get_item_names (lua_State *L) const
{
	std::unique_ptr<std::vector<std::string>> item_names
		(new std::vector<std::string>);
	for (auto &item : _inventory) {
		item_names->push_back (lua_util::get_item_name(L, item.type));
		if (item.amount > 1) {
			item_names->back() += " (x" + std::to_string (item.amount) + ")";
		}
	}
	return item_names;
}

bool Being::equip_weapon (unsigned int index)
{
	if (_inventory.size() > index) {
		// TODO: check if item is a weapon.
		if (_weapon.type != "null") {
			_inventory.push_back (_weapon);
		}
		_weapon = _inventory[index];
		_inventory.erase (_inventory.begin() + index);
		return true;
	} else {
		return false;
	}
}

std::unique_ptr<std::vector<item_data>> Being::get_equipment () const
{
	std::unique_ptr<std::vector<item_data>> equipment (new std::vector<item_data>);

	/* currently only weapons exist... */
	if (_weapon.type != "null") {
		equipment->push_back (_weapon);
	}

	return equipment;
}
