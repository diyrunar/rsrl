/* Exceptions used by RSRL.
 * All of them derive from std::exception. */

#include <exception>
#include <string>

/* Thrown when someone tries to move into a blocked tile. */
class CantGoThere: public std::exception
{
	public:
		const char *what () const noexcept override;
};

/* Thrown when something tries to access coordinates outside the map. */
class OutOfBounds: public std::exception
{
	public:
		const char *what () const noexcept override;
};

/* Thrown when there's an error in the controls handling. */
class ControlError: public std::exception
{
	public:
		const char *what () const noexcept override;
};
