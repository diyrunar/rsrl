/* Miscellaneous utilities that don't really belong anywhere else. */

#include <cstdlib> /* for abs() */
#include "util.h"

int get_array_pos (int x, int y, int array_w)
{
	return y * array_w + x;
}

int manhattan (int x1, int y1, int x2, int y2)
{
	return abs(x1 - x2) + abs(y1 - y2);
}
