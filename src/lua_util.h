/* A few mostly project-specific functions to make accessing Lua data easier. */

#ifndef LUA_UTIL_H
#define LUA_UTIL_H

#include <string>

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

namespace lua_util
{
	/* Access the Lua registry */
	void  add_to_registry   (lua_State *L, const std::string &name, void *data);
	void  add_to_registry   (lua_State *L, const char        *name, void *data);
	void *get_from_registry (lua_State *L, const std::string &name);
	void *get_from_registry (lua_State *L, const char        *name);

	/* Item handling */
	std::string get_item_name (lua_State *L, const std::string &item);
	std::string get_item_name (lua_State *L, const char        *item);
}

#endif /* end of include guard: LUA_UTIL_H */
