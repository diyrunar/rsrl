#ifndef UTIL_H
#define UTIL_H

/* Miscellaneous utilities that don't really belong anywhere else. */

/* Get a 2D position in an 1D array (such as _game_view) */
int get_array_pos (int x, int y, int array_w);

/* Calculate Manhattan distance between (x1, y1) and (x2, y2) */
int manhattan (int x1, int y1, int x2, int y2);

#endif /* end of include guard: UTIL_H */
