#include <vector>
#include <string>
#include "control.h"
#include "lua_util.h"
#include "exception.h"

/* Shows the player a list of items on the player's tile and returns the item
 * that the player chose. */
static int choose_item_to_pick_up (Map &map,
								   GraphicsSystem &gfx_system,
								   lua_State *L,
								   unsigned int player_id)
{
	/* Get list of items on the player's tile */
	being_coords player_coords = map.get_coords (player_id);
	auto item_list = map.get_items_on_tile (player_coords.x, player_coords.y);
	if (item_list->size() < 1) { // no items on tile
		std::string msg ("There is nothing to pick up here.");
		gfx_system.add_message (msg);
		return -1;
	}

	std::vector<std::string> item_names;
	for (auto &item : *item_list) {
		item_names.push_back (lua_util::get_item_name(L, item.type));
		if (item.amount > 1) {
			item_names.back() += " (" + std::to_string (item.amount) + ")";
		}
	}

	/* Show menu */
	std::string title ("What do you want to pick up?");
	return gfx_system.show_menu (title, item_names);
}

/* Chooses an item from the player's inventory for whatever purpose. */
static int choose_item_from_inventory (Being &player,
									   GraphicsSystem &gfx_system,
									   lua_State *L,
									   std::string &title)
{
	auto item_names = player.get_item_names(L);
	return gfx_system.show_menu(title, *item_names);
}

std::function<void (unsigned int)> make_action (sf::Event &event,
												BeingList &beings,
												Map &map,
												GraphicsSystem &gfx_system,
												lua_State *L,
												unsigned int player_id)
{
	if (event.type != sf::Event::KeyPressed) {
		return nullptr;
	}

	if (event.key.code == sf::Keyboard::Up
	 || event.key.code == sf::Keyboard::Down
	 || event.key.code == sf::Keyboard::Left
	 || event.key.code == sf::Keyboard::Right) {
		MoveAction tmp(map, gfx_system, event);
		return std::function<void (unsigned int)>(tmp);
	} else if (event.key.code == sf::Keyboard::I) {
		return [&, L](unsigned int id) {
			auto item_list = beings.at(id).get_items();
			std::vector<std::string> item_names;
			for (auto &item : *item_list) {
				item_names.push_back (lua_util::get_item_name(L, item.type));
				if (item.amount > 1) {
					item_names.back() += " (" + std::to_string (item.amount) + ")";
				}
			}
			std::string title ("Your inventory contains:");
			gfx_system.show_menu (title, item_names);
		};
	} else if (event.key.code == sf::Keyboard::Comma) {
		/* the lambda only picks up the item; choosing the item is done here. */
		int item = choose_item_to_pick_up(map, gfx_system, L, player_id);
		if (item >= 0) {
			return [&beings, &map, item](unsigned int id) {
				being_coords coords = map.get_coords(id);
				item_data tmp = *(map.remove_item(coords.x, coords.y, item));
				beings.at(id).add_item(tmp.type, tmp.amount);
			};
		} else {
			return nullptr;
		}
	} else if (event.key.code == sf::Keyboard::D) {
		std::string title("What do you want to drop?");
		int item = choose_item_from_inventory(beings.at(player_id), gfx_system, L, title);
		if (item >= 0) {
			return [&beings, &map, item](unsigned int id) {
				item_data tmp = *(beings.at(id).remove_item(item));

				/* Add item on map */
				being_coords coords = map.get_coords(id);
				map.add_item(tmp.type, tmp.amount, coords.x, coords.y);
			};
		} else {
			return nullptr;
		}
	} else if (event.key.code == sf::Keyboard::E) {
		std::string title("What do you want to eat?");
		int item = choose_item_from_inventory(beings.at(player_id), gfx_system, L, title);
		if (item >= 0) {
			return [&beings, L, item](unsigned int id) {
				auto item_list = beings.at(id).get_items();
				std::string item_type = item_list->at(item).type;
				lua_getglobal(L, "eat_item");
				lua_pushnumber(L, id);
				lua_pushstring(L, item_type.c_str());
				lua_pcall(L, 2, 0, 0);
			};
		} else {
			return nullptr;
		}
	} else if (event.key.code == sf::Keyboard::W) {
		std::string title("What do you want to equip?");
		int item = choose_item_from_inventory(beings.at(player_id), gfx_system, L, title);
		if (item >= 0) {
			return[&beings, &gfx_system, item](unsigned int id) {
				bool success = beings.at(id).equip_weapon(item);
				if (!success) {
					std::string msg("You can't equip that item.");
					gfx_system.add_message(msg);
				}
			};
		} else {
			return nullptr;
		}
	} else {
		return nullptr;
	}
}

MoveAction::MoveAction (Map &map, GraphicsSystem &gfx_system, sf::Event &event):
	_map(map),
	_gfx_system(gfx_system),
	_dx(0),
	_dy(0)
{
	if (event.type != sf::Event::KeyPressed) {
		throw ControlError();
	}

	switch (event.key.code) {
		case sf::Keyboard::Up:
			_dy = -1;
			break;
		case sf::Keyboard::Down:
			_dy = 1;
			break;
		case sf::Keyboard::Left:
			_dx = -1;
			break;
		case sf::Keyboard::Right:
			_dx = 1;
			break;
		default:
			throw ControlError();
			break;
	}

}

void MoveAction::operator()(unsigned int id) const
{
	try {
		_map.move_being(id, _dx, _dy);
	} catch (CantGoThere &e) {
		std::string msg("You can't go there.");
		_gfx_system.add_message(msg);
	}
}
