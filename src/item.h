/* This struct stores all information about items that C++ needs.
 * Most information is stored in Lua. */

#ifndef ITEM_H
#define ITEM_H

struct item_data
{
	std::string type;
	unsigned int amount;
};

#endif /* end of include guard: ITEM_H */
