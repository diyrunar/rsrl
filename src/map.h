#ifndef MAP_H
#define MAP_H
/* This class contains a single map. */

#include <string>
#include <vector>
#include <random>
#include <map>
#include <memory>

#include "being.h"
#include "id_gen.h"
#include "item.h"

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

/* Tile types (shouldn't be an enum, but works for now) */
enum class Tile
{
	FLOOR,
	WALL
};

/* This struct stores a being's ID and its coordinates. */
struct being_coords
{
	unsigned int id;
	int x;
	int y;
};

class Map
{
	public:
		/* Constructor */
		Map (std::mt19937 &rng,
		     BeingList &beings,
		     IDGenerator &id_gen,
		     lua_State *L,
		     unsigned int player_id);

		/* Get a specific tile's symbol */
		char get_symbol (int x, int y) const; 
		/* Can a being move on a specific square? */
		bool is_open (int x, int y) const;

		/* Are the given coordinates inside the Map? */
		bool is_in_bounds (int x, int y) const;

		/* Return list of beings on this map */
		std::vector<being_coords>& get_beings ();

		/* Get coordinates of a Being with the given ID */
		being_coords get_coords (unsigned int id) const;

		/* Moves a Being the given amount of squares */
		void move_being (unsigned int id, int dx, int dy);

		/* If there is a Being on the given tile, returns its ID;
		 * otherwise, returns 0. */
		unsigned int get_being_on_tile (int x, int y) const;

		/* Removes a Being from the map */
		void remove_being (unsigned int id);

		/* Adds a Being to the map */
		void add_being (unsigned int id, int x, int y);

		/* Returns all items on a given tile */
		std::unique_ptr<std::vector<item_data>>
			get_items_on_tile (int x, int y) const;

		/* Adds an item to the map */
		void add_item (const std::string &item, unsigned int amount, int x, int y);

		/* Removes an item from the map and returns the removed item */
		std::unique_ptr<item_data> remove_item (int x, int y, unsigned int index);

		/* Returns every Being that is at most r squares away from the given
		 * square */
		std::unique_ptr<std::vector<unsigned int>> get_beings_in_radius
			(int x, int y, int r);
	
		/* Finds a path between (x1, y1) and (x2, y2) */
		std::unique_ptr<std::pair<int, int>> find_path
			(int x1, int y1, int x2, int y2) const;
	private:
		/* Map size */
		int _width;
		int _height;

		/* Map tiles */
		std::vector<Tile> _tiles;

		/* Beings that are on this map */
		std::vector<being_coords> _local_beings;

		/* Items that are on this map */
		std::map<unsigned int, std::vector<item_data>> _items;

		/* Map of all Beings in the game */
		BeingList &_beings;

		/* ID generator */
		IDGenerator &_id_gen;

		/* Lua VM */
		lua_State *_L;

		/* Returns all tiles surrounding (x, y) */
		std::unique_ptr<std::vector<int>>
			get_surrounding_tiles (int x, int y) const;
};

#endif /* end of include guard: MAP_H */
