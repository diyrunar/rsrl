#include "exception.h"

const char *CantGoThere::what () const noexcept
{
	return "The target tile is blocked.";
}

const char *OutOfBounds::what () const noexcept
{
	return "Coordinates are outside the map.";
}

const char *ControlError::what () const noexcept
{
	return "Error in controls handling.";
}
