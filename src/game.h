#ifndef GAME_H
#define GAME_H

/* Game class for RSRL.
 * This is where most of the action happens. */

#include <vector>
#include <random>
#include <map>

#include <SFML/Graphics.hpp>

#include "map.h"
#include "being.h"
#include "graphicssystem.h"
#include "id_gen.h"

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

class Game
{
	public:
		/* Constructor */
		Game (sf::RenderWindow &game_win, lua_State *L);

		/* Destructor */
		~Game ();

		/* Enter the game loop */
		void run ();

		/* Do monsters' turns */
		void process_monsters ();

	private:
		/* Game window (only used for input, graphics are in another class) */
		sf::RenderWindow &_game_win;

		/* Random number generator */
		std::mt19937 _rng;

		/* Graphics system */
		GraphicsSystem _gfx_system;

		/* Lua VM */
		lua_State *_L;

		/* ID generator */
		IDGenerator _id_gen;

		/* Map of all Beings in the game */
		BeingList _beings;

		/* ID number of the player's Being */
		unsigned int _player_id;

		/* Map */
		Map *_map;
};

#endif /* end of include guard: GAME_H */
