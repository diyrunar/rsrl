/* ID generator. Generates a unique ID for every Being in the game. */

#ifndef ID_GEN_H
#define ID_GEN_H

class IDGenerator
{
	public:
		/* Constructor */
		IDGenerator (unsigned int start = 1) : _next_id (start) {}
		
		/* Get next free ID and increase counter */
		unsigned int new_id () { return _next_id++; }

	private:
		/* Next free ID */
		unsigned int _next_id;
}; 

#endif /* end of include guard: ID_GEN_H */
