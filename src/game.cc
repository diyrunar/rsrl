/* Game class for RSRL.
 * This is where most of the action happens. */

#include <ctime>
#include <string>

#include "game.h"
#include "control.h"
#include "util.h"
#include "lua_util.h"

/* Constructor */
Game::Game (sf::RenderWindow &game_win, lua_State *L) :
	_game_win (game_win),
	_rng (time(0)),
	_gfx_system (game_win),
	_L (L),
	_id_gen (),
	_player_id (_id_gen.new_id())
{
	/* Generate the map */
	_map = new Map(_rng, _beings, _id_gen, _L, _player_id);

	/* Add some addresses to the Lua registry */
	lua_util::add_to_registry (_L, "gfx_system", (void*)&_gfx_system);
	lua_util::add_to_registry (_L, "beings", (void*)&_beings);
	lua_util::add_to_registry (_L, "map", (void*)_map);
	lua_util::add_to_registry (_L, "id_gen", (void*)&_id_gen);

	/* Make player character */
	std::string tmp ("player");
	_beings.emplace (_player_id, Being(_L, tmp));
}

/* Destructor */
Game::~Game ()
{
	delete _map;
}

/* Game loop */
void Game::run ()
{
	bool cont = true;
	while (cont) {
		_gfx_system.draw_map (*_map, _beings, _L);
		_gfx_system.print_messages();
		_gfx_system.draw_stats (_beings.at(_player_id).get_hp(),
		                        _beings.at(_player_id).get_maxhp());
		_gfx_system.display();
		sf::Event ev;
		bool player_turn_done = false;
		while (_game_win.pollEvent (ev)) {
			if (ev.type == sf::Event::KeyPressed
			 && ev.key.code == sf::Keyboard::Q) {
				player_turn_done = true;
				return;
			} else {
				/* action is a lambda */
				auto action = make_action (ev, _beings, *_map,
						_gfx_system, _L, _player_id);
				if (action != nullptr) {
					action(_player_id);
					player_turn_done = true;
				}
			}
		}

		if (player_turn_done) {
			process_monsters();
		}

		/* Clear dead beings */
		auto it = _beings.cbegin();
		while (it != _beings.cend()) {
			if (it->second.is_dead()) {
				_map->remove_being (it->first);
				it = _beings.erase (it);
			} else {
				it++;
			}
		}

		/* Is player dead? */
		if (_beings.count(_player_id) < 1) {
			cont = false;
			std::string msg ("You die... (press Q to quit)");
			_gfx_system.add_message (msg);
			_gfx_system.draw_map (*_map, _beings, _L);
			_gfx_system.print_messages();
		}
	}
}

void Game::process_monsters ()
{
	for (auto &it : _beings) {
		if (it.first != _player_id) {
			lua_getglobal (_L, "do_mon_ai");
			lua_pushnumber (_L, it.first);
			lua_pcall (_L, 1, 0, 0);
		}
	}
}
