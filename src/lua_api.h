/* Functions that can be called from Lua. */

#ifndef LUA_API_H
#define LUA_API_H

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

/* Prints a message on the screen */
int rs_add_message (lua_State *L);

/* Functions for handling Being stats */
int rs_get_hp (lua_State *L);
int rs_get_maxhp (lua_State *L);
int rs_adjust_hp (lua_State *L);

/* Move player a given number of squares */
int rs_move (lua_State *L);

/* Add a new monster to the current map */
int rs_add_mon (lua_State *L);

/* Get list of Beings that are within a certain distance of a square */
int rs_get_in_radius (lua_State *L);

/* Return the player's ID number */
int rs_get_player (lua_State *L);

/* Returns a Being's coordinates */
int rs_get_coords (lua_State *L);

/* Find a path */
int rs_get_next_move (lua_State *L);

/* Returns the Being on a specific tile (or nil if it's empty) */
int rs_get_being_on_tile (lua_State *L);

/* Returns all items that a Being has equipped */
int rs_get_equipment (lua_State *L);

/* Checks if a tile is open */
int rs_is_tile_open (lua_State *L);

/* A module of all the API functions */
const struct luaL_Reg rs_lib [] =
{
	{ "add_message"     , rs_add_message      },
	{ "get_hp"          , rs_get_hp           },
	{ "get_maxhp"       , rs_get_maxhp        },
	{ "adjust_hp"       , rs_adjust_hp        },
	{ "move"            , rs_move             },
	{ "add_mon"         , rs_add_mon          },
	{ "get_in_radius"   , rs_get_in_radius    },
	{ "get_player"      , rs_get_player       },
	{ "get_coords"      , rs_get_coords       },
	{ "get_next_move"   , rs_get_next_move    },
	{"get_being_on_tile", rs_get_being_on_tile},
	{ "get_equipment"   , rs_get_equipment    },
	{ "is_tile_open"    , rs_is_tile_open     },
	{ NULL              , NULL                }
};

#endif /* end of include guard: LUA_API_H */
