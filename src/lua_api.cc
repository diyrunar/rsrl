#include <string>
#include <map>
#include <memory>

#include "lua_api.h"
#include "graphicssystem.h"
#include "being.h"
#include "map.h"
#include "id_gen.h"
#include "lua_util.h"

/* Print a message on the screen */
int rs_add_message (lua_State *L)
{
	std::string msg (lua_tostring (L, 1));

	/* Get a reference to GraphicsSystem */
	GraphicsSystem *gfx_system =
		(GraphicsSystem*) lua_util::get_from_registry (L, "gfx_system");

	gfx_system->add_message (msg);

	return 0;
}

/* Returns a being's HP */
int rs_get_hp (lua_State *L)
{
	if (lua_isnumber (L, -1)) {
		int id = lua_tointeger (L, -1);

		auto beings = (BeingList*) lua_util::get_from_registry (L, "beings");

		auto b = beings->find(id);
		if (b != beings->end()) { // A being with the given ID was found
			lua_pushnumber (L, (b->second).get_hp());
			return 1;
		} else {
			return 0; // no being was found
		}
	} else {
		return 0; // argument wasn't a number
	}
}

int rs_get_maxhp (lua_State *L)
{
	if (lua_isnumber (L, -1)) {
		int id = lua_tointeger (L, -1);

		auto beings = (BeingList*) lua_util::get_from_registry (L, "beings");

		auto b = beings->find(id);
		if (b != beings->end()) { // A being with the given ID was found
			lua_pushnumber (L, (b->second).get_maxhp());
			return 1;
		} else {
			return 0; // no being was found
		}
	} else {
		return 0; // argument wasn't a number
	}
}

/* Increases a Being's HP by the given amount
 * (negative values can be used to decrease HP) */
int rs_adjust_hp (lua_State *L)
{
	if (lua_isnumber (L, -1) && lua_isnumber (L, -2)) {
		int id = lua_tointeger (L, -2);
		int amount = lua_tointeger (L, -1);

		auto beings = (BeingList*) lua_util::get_from_registry (L, "beings");

		auto b = beings->find(id);
		if (b != beings->end()) { // A being with the given ID was found
			(b->second).adjust_hp (amount);
		}
	}

	return 0;
}

int rs_move (lua_State *L)
{
	unsigned int id = luaL_checkinteger (L, 1);
	int dx = luaL_checkinteger (L, 2);
	int dy = luaL_checkinteger (L, 3);

	auto map = (Map*) lua_util::get_from_registry (L, "map");

	map->move_being (id, dx, dy);

	return 0;
}

/* Adds a new monster to the current map.
 * Parameters: monster type, x coordinate, y coordinate. */
int rs_add_mon (lua_State *L)
{
	if (lua_isstring(L, 1) && lua_isnumber(L, 2) && lua_isnumber(L, 3)) {
		std::string type (lua_tostring(L, 1));
		int x (lua_tointeger(L, 2));
		int y (lua_tointeger(L, 3));

		/* Add new Being to being list */
		auto beings = (BeingList*) lua_util::get_from_registry (L, "beings");
		auto id_gen = (IDGenerator*) lua_util::get_from_registry (L, "id_gen");
		unsigned int id = id_gen->new_id();
		beings->emplace (id, Being(L, type));

		/* Add new Being to current map */
		auto map = (Map*) lua_util::get_from_registry (L, "map");
		map->add_being (id, x, y);

		/* Return new Being's ID */
		lua_pushnumber (L, id);
		return 1;
	} else { /* invalid arguments */
		lua_pushnil (L);
		return 1;
	}
}

/* Parameters: x, y, radius */
int rs_get_in_radius (lua_State *L)
{
	if (lua_isnumber(L, 1) && lua_isnumber(L, 2) && lua_isnumber(L, 3)) {
		const int x = lua_tonumber (L, 1);
		const int y = lua_tonumber (L, 2);
		const int r = lua_tonumber (L, 3);

		auto map = (Map*) lua_util::get_from_registry (L, "map");

		std::unique_ptr<std::vector<unsigned int>> beings =
			map->get_beings_in_radius (x, y, r);

		/* Create a new Lua table and push the Being IDs in it */
		lua_newtable (L);
		int index = 1;
		for (unsigned int &id : *beings) {
			lua_pushnumber (L, id);
			lua_rawseti (L, -2, index);
			index++;
		}

		return 1; /* table of Being IDs */
	} else { /* invalid arguments */
		lua_pushnil (L);
		return 1;
	}
}

int rs_get_player (lua_State *L)
{
	lua_pushnumber (L, 1);
	return 1;
}

int rs_get_coords (lua_State *L)
{
	if (lua_isnumber(L, 1)) {
		const unsigned int id = lua_tonumber (L, 1);

		auto map = (Map*) lua_util::get_from_registry (L, "map");

		const being_coords coords = map->get_coords (id);
		lua_pushnumber (L, coords.x);
		lua_pushnumber (L, coords.y);
		return 2;
	} else { /* invalid argument */
		return 0;
	}
}

int rs_get_next_move (lua_State *L) {
	int x1, y1, x2, y2;
	if (lua_isnumber(L, 1)
	 && lua_isnumber(L, 2)
	 && lua_isnumber(L, 3)
	 && lua_isnumber(L, 4)) {
		x1 = lua_tonumber (L, 1);
		y1 = lua_tonumber (L, 2);
		x2 = lua_tonumber (L, 3);
		y2 = lua_tonumber (L, 4);
	} else {
		return 0;
	}

	auto map = (Map*) lua_util::get_from_registry (L, "map");

	auto next_move = map->find_path (x1, y1, x2, y2);
	if (next_move) {
		lua_pushnumber (L, next_move->first);
		lua_pushnumber (L, next_move->second);
		return 2;
	} else {
		return 0;
	}
}

int rs_get_being_on_tile (lua_State *L)
{
	int x, y;
	if (lua_isnumber(L, 1) && lua_isnumber(L, 2)) {
		x = lua_tonumber(L, 1);
		y = lua_tonumber(L, 2);
	} else {
		return 0;
	}

	auto map = (Map*) lua_util::get_from_registry (L, "map");

	int id = map->get_being_on_tile (x, y);
	if (id) {
		lua_pushnumber (L, id);
		return 1;
	} else {
		return 0;
	}
}

int rs_get_equipment (lua_State *L)
{
	if (!lua_isnumber(L, 1)) {
		return 0;
	}

	const unsigned int id = lua_tonumber (L, 1);

	auto beings = (BeingList*) lua_util::get_from_registry (L, "beings");

	auto equipment = beings->at(id).get_equipment();
	lua_newtable (L); // all the equipment
	int index = 1;
	for (const item_data &item : *equipment) {
		lua_newtable (L); // a single item
		lua_pushstring (L, item.type.c_str());
		lua_setfield (L, -2, "type");
		lua_pushnumber (L, item.amount);
		lua_setfield (L, -2, "amount");
		lua_rawseti (L, -2, index++);
	}

	return 1;
}

int rs_is_tile_open (lua_State *L)
{
	if (!(lua_isnumber(L, 1) && lua_isnumber(L, 2))) {
		lua_pushboolean (L, 0);
		return 1;
	}

	int x = lua_tonumber (L, 1);
	int y = lua_tonumber (L, 2);

	auto map = (Map*) lua_util::get_from_registry (L, "map");

	lua_pushboolean (L, map->is_open(x, y));
	return 1;
}
