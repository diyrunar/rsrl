#include <string>

#include "graphicssystem.h"
#include "util.h"

static const int CHAR_W = 8;
static const int CHAR_H = 12;

/* Constructor */
GraphicsSystem::GraphicsSystem (sf::RenderWindow &game_win) :
	_game_font (),
	_game_win (game_win)
{
	_game_font.loadFromFile ("DSM.ttf");

	/* Init the game view */
	for (unsigned int i = 0; i < _game_view.size(); ++i) {
		_game_view[i].setFont (_game_font);
		_game_view[i].setCharacterSize (12);
		_game_view[i].setColor (sf::Color::White);
		_game_view[i].setString (" ");
		_game_view[i].setPosition (i % 60 * CHAR_W, i / 60 * CHAR_H);
	}

	/* Init message box */
	for (unsigned int i = 0; i < _messages.size(); ++i) {
		_messages[i].setFont (_game_font);
		_messages[i].setCharacterSize (16);
		_messages[i].setColor (sf::Color::White);
		_messages[i].setString ("");
		_messages[i].setPosition (0, CHAR_H * VIEW_H + 16 * i + 10);
	}
}

void GraphicsSystem::draw_map (Map &map, BeingList &beings, lua_State *L)
{
	sf::Text text;
	text.setFont (_game_font);
	text.setCharacterSize (32);
	text.setColor (sf::Color::White);
	text.setString ("Press Q to exit!");
	text.setPosition (50, 50);

	_game_win.clear (sf::Color::Black);
	_game_win.draw (text);

	/* Draw map */
	for (int i = 0; i < 25; ++i) {
		for (int j = 0; j < 60; ++j) {
			draw_char_in_view (j, i, map.get_symbol (j, i));
		}
	}

	/* Draw monsters */
	auto being_ids = map.get_beings();
	for (being_coords &c : being_ids) {
		/* Get monster symbol from Lua */
		std::string being_type = beings.at(c.id).get_type();
		lua_getglobal (L, "get_mon_symbol");
		lua_pushstring (L, being_type.c_str());
		lua_pcall (L, 1, 1, 0);
		std::string symbol = lua_tostring (L, -1);
		lua_pop (L, 1); /* remove the symbol from Lua stack */
		draw_char_in_view (c.x, c.y, symbol[0]);
	}

	/* Draw the game view */
	for (sf::Text &t : _game_view) {
		_game_win.draw (t);
	}
}

void GraphicsSystem::draw_stats (int hp, int maxhp)
{
	sf::Text hp_text;
	hp_text.setFont (_game_font);
	hp_text.setCharacterSize (16);
	hp_text.setColor (sf::Color::White);
	hp_text.setString ("Press Q to exit!");
	hp_text.setPosition (CHAR_W * VIEW_W + 10, 20);
	hp_text.setString (std::string ("HP: ") + std::to_string(hp)
			+ "/" + std::to_string(maxhp));
	_game_win.draw (hp_text);
}

void GraphicsSystem::add_message (std::string &msg)
{
	_msg_queue.push_back (msg);
}

/* Print messages on queue on screen */
void GraphicsSystem::print_messages ()
{
	while (!_msg_queue.empty()) {
		/* Move all existing messages up one line */
		for (unsigned int i = 0; i < _messages.size() - 1; ++i) {
			_messages[i].setString (_messages[i+1].getString());
		}

		/* Set last message to the new message */
		_messages.back().setString (_msg_queue.front());

		/* Remove message from queue */
		_msg_queue.erase (_msg_queue.begin());
	}

	/* Draw messages on screen */
	for (sf::Text &msg : _messages) {
		_game_win.draw (msg);
	}
}

/* Display everything on screen */
void GraphicsSystem::display ()
{
	_game_win.display();
}

void GraphicsSystem::draw_char_in_view (int x, int y, char c)
{
	int pos = get_array_pos (x, y, VIEW_W);
	_game_view[pos].setString (std::string (1, c));
}

int GraphicsSystem::show_menu (std::string &title, std::vector<std::string> &options)
{
	sf::Text title_text;
	title_text.setFont (_game_font);
	title_text.setCharacterSize (32);
	title_text.setColor (sf::Color::White);
	title_text.setString (title);
	title_text.setPosition (50, 0);

	std::vector<sf::Text> choices;
	int ypos = 32;
	for (auto& opt : options) {
		choices.push_back (sf::Text());
		choices.back().setFont (_game_font);
		choices.back().setCharacterSize (24);
		choices.back().setColor (sf::Color::White);
		choices.back().setString (opt);
		choices.back().setPosition (0, ypos);
		ypos += choices.back().getGlobalBounds().height;
	}

	unsigned int curropt = 0; // currently selected option
	for (;;) {
		/* Render everything */
		_game_win.clear (sf::Color::Black);

		/* Draw the texts */
		for (unsigned int i = 0; i < choices.size(); ++i) {
			/* Set color */
			if (i == curropt) {
				choices[i].setColor (sf::Color::Red);
			} else {
				choices[i].setColor (sf::Color::White);
			}

			/* Draw */
			_game_win.draw (choices[i]);
		}
		_game_win.draw (title_text);

		_game_win.display();

		/* Get input */
		sf::Event event;
		while (_game_win.pollEvent (event)) {
			if (event.type == sf::Event::KeyPressed) {
				switch (event.key.code) {
					case sf::Keyboard::Up:
						curropt = (--curropt) % choices.size();
						break;
					case sf::Keyboard::Down:
						curropt = (++curropt) % choices.size();
						break;
					case sf::Keyboard::Return:
						return curropt;
						break;
					case sf::Keyboard::Escape:
						return -1;
						break;
					default:
						break;
				}
			}
		}
	}
}
