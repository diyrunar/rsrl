/* Main file for RSRL.
 * This file contains the main menu (though it probably shouldn't). */

#include <string>
#include <vector>
#include <ctime> /* for seeding Lua RNG */

#include <SFML/Graphics.hpp>

#include "game.h"
#include "lua_api.h"

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

int main ()
{
	/* Initialize graphics */
	sf::RenderWindow game_win (sf::VideoMode(640, 480, 32), "RSRL");
	sf::Font font;
	font.loadFromFile ("DSM.ttf");

	/* Start Lua VM */
	lua_State *L = luaL_newstate();
	luaL_openlibs (L);

	/* Register API functions in Lua */
	luaL_newlib (L, rs_lib);
	lua_setglobal (L, "rs");

	/* Load Lua files */
	luaL_dofile (L, "lua/combat.lua");
	luaL_dofile (L, "lua/mapgen.lua");
	luaL_dofile (L, "lua/monster.lua");
	luaL_dofile (L, "lua/item.lua");
	luaL_dofile (L, "lua/ai.lua");

	/* Seed Lua RNG
	 * TODO: make Lua use the RNG in Game class */
	lua_getglobal (L, "math");
	lua_getfield (L, -1, "randomseed");
	lua_pushnumber (L, time(0));
	lua_pcall (L, 1, 0, 0);
	lua_pop (L, 1); /* get rid of the math table */

	/* Create the main menu */
	const std::vector<std::string> CHOICES =
	{
		"New game",
		"Quit"
	};

	std::vector<sf::Text*> opts;
	int ypos = 0;
	for (auto& i : CHOICES) {
		sf::Text *tmp = new sf::Text;
		tmp->setFont (font);
		tmp->setCharacterSize (32);
		tmp->setColor (sf::Color::White);
		tmp->setString (i);
		tmp->setPosition (0, ypos);
		ypos += tmp->getGlobalBounds().height;
		opts.push_back (tmp);
	}

	/* Copyright notice */
	const std::string copyright_notice = std::string("")
	+"This is a free fangame, (C) The Runar 2014.\n"
	+"This game is in no way affiliated with "
	+"Jagex or RuneScape.\n\n"
	+"This game is free software;\n"
	+"see LICENSE.txt for more information.";
	sf::Text copyright;
	copyright.setFont (font);
	copyright.setCharacterSize (16);
	copyright.setColor (sf::Color::White);
	copyright.setString (copyright_notice);
	copyright.setPosition
		(0, game_win.getSize().y - copyright.getGlobalBounds().height - 20);

	/* Main menu loop */
	unsigned int curropt = 0; // currently selected option
	while (game_win.isOpen()) {
		/* Render everything */
		game_win.clear (sf::Color::Blue);

		/* Draw the texts */
		for (unsigned int i = 0; i < opts.size(); ++i) {
			/* Set color */
			if (i == curropt) {
				opts[i]->setColor (sf::Color::Red);
			} else {
				opts[i]->setColor (sf::Color::White);
			}

			/* Draw */
			game_win.draw (*(opts[i]));
		}

		game_win.draw (copyright);

		game_win.display();

		/* Get input */
		sf::Event event;
		while (game_win.pollEvent (event)) {
			if (event.type == sf::Event::Closed) {
				game_win.close();
			}

			/* Move up or down */
			else if (event.type == sf::Event::KeyPressed) {
				switch (event.key.code) {
					case sf::Keyboard::Up:
						curropt = (--curropt) % CHOICES.size();
						break;
					case sf::Keyboard::Down:
						curropt = (++curropt) % CHOICES.size();
						break;
					case sf::Keyboard::Return:
						if (curropt == 0) {
							/* Start the game */
							Game g (game_win, L);
							g.run();
						}
						else if (curropt == 1) {
							game_win.close();
						}
						break;
					default:
						break;
				}
			}
		}
	}

	/* Clean up */
	opts.clear();
	lua_close (L);
}
