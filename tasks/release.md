This file contains a list of tasks that should be completed before RSRL is
ready for release.

Combat
------

* [] Accuracy calculation
* [] Better damage calculation
* [] XP, leveling up
* [] Ranged
* [] Magic

Items
-----

* Equipment other than weapons
	* [] Keep track of already filled slots
	* [] Different monsters have different slots available --- for example, a
	a goblin should not be able to wear any human armor
	* [] Two-handed weapons
* [] Item rarity: some items are generated more often than others
* [] Colored items

Maps
----

* Multiple maps
	* [] Allow the engine to switch to another map
	* [] When player goes down the stairs, the next level should be generated
	unless it already exists
	* [] The deeper the player goes, the more difficult the maps are

Monsters
--------

* Monster stats
	* [] Combat stats
	* [] Different equipment slots
* A smarter AI
	* [] Flee when low on HP
	* [] Explore the dungeon randomly if there is no target
* Monsters using items
	* [] Eat food when low on HP
	* [] Equip items if it's useful
	* [] Pick up items from the ground
* A better way of sending commands to monsters. There should be an abstract
base class "Command" and then a subclass for each possible action.
	* [] Add a command parser to the Being class that handles movement
	* [] Add item handling commands
	* [] Parse commands from Lua
* [] Colored monsters

Saving / loading
----------------

Save files will be implemented as Lua scripts.

* [] Save the current game state
* [] Add the necessary functions to restore game state (allow the game to
"start" on a different level etc.)

Miscellaneous
-------------

* [] Colored messages
