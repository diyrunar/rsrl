RSRL development tasks
======================

This file contains a list of features that RSRL supports or will eventually
support.

Graphics
--------

* [2014-03-01] Open the game window
* [2014-03-01] Draw something on the screen
* [2014-03-01] Add a function to draw a single character
* [2014-03-02] Draw the map on the screen
* [2014-03-05] Add support for messages
* [2014-03-05] Move everything graphics-related to its own class

Map
---

* [2014-03-02] Add walls
* [2014-03-04] Generate random walls
* [2014-04-16] Generate a simple random map
* [2014-04-12] Generate items randomly

Player
------

* [2014-03-02] Draw the player character
* [2014-03-02] Add movement for player
* [2014-03-07] Add stats (at least HP)
* [2014-03-08] Allow player to attack monsters
* [2014-03-03] Move player in Map class

Monsters
--------

* [2014-03-04] Add an enemy creature
* [2014-03-08] Add HP to monsters so they can die
* [2014-03-29] Give monsters (basic) AI
* [2014-04-02] Add pathfinding (Dijkstra)
* [#] (Add item drops) Postponing this. It may be a better idea to add the
  "drops" into a monster's inventory at the time the monster is created so it
  can use the items in combat.

Items
-----

* [2014-03-29] Add items that you can drop and pick up
* [2014-04-11] Add equipment
* [2014-03-29] Add food

Lua
---

* [2014-03-05] Add a Lua VM
* [2014-03-06] Allow Lua to write messages on screen
* [2014-03-07] Allow Lua to read player's stats
* [2014-03-07] Allow Lua to control the player
* [2014-03-07] Allow Lua to control the monsters
* [2014-03-17] Allow Lua to create the map

Misc
----

* [2014-03-03] Add RNG in Game class
* [2014-03-06] Use references instead of pointers for game window
