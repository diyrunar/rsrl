-- monster.lua
-- Monster definitions and a few helper functions.

monster = {
	player = {
		symbol = '@',
		maxhp = 10,
	},

	test_monster = {
		symbol = 'M',
		maxhp = 5,
	}
}

-- Functions for using the monsters

function get_mon_symbol (mon)
	if monster[mon] then
		local sym = monster[mon].symbol
		if sym then return sym end
	end
end

function get_mon_maxhp (mon)
	if monster[mon] then
		local hp = monster[mon].maxhp
		if hp then return hp end
	end
end
