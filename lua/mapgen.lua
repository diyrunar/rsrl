local WALL = '#'
local FLOOR = '.'

-- Counts how many of the eight tiles surrounding (row, col) are walls.
local function count_neighbors (map, row, col)
	local count = 0
	for i = -1, 1 do
		for j = -1, 1 do
			if map[row+i] and map[row+i][col+j] and map[row+i][col+j] == WALL then
				count = count + 1
			end
		end
	end
	return count
end

-- Does one iteration of the Game of Life with the given rules.
-- An empty cell is converted into a wall if at least b of its neighbors
-- are walls. A wall stays a wall if at least s of its neighbors are walls.
-- Otherwise the cell becomes empty.
local function process_life (map, b, s)
	local new_map = {}
	local neighbors = {}

	-- count neighbors
	for row = 1, #map do
		neighbors[row] = {}
		for col = 1, #map[row] do
			neighbors[row][col] = count_neighbors (map, row, col)
		end
	end

	-- create new map
	for row = 1, #map do
		new_map[row] = {}
		for col = 1, #map[row] do
			if (map[row][col] == WALL and neighbors[row][col] >= s)
				or (map[row][col] == FLOOR and neighbors[row][col] >= b) then
				new_map[row][col] = WALL
			else
				new_map[row][col] = FLOOR
			end
		end
	end

	return new_map
end

-- This function uses cellular automata to create a cave-like map.
-- The principle is similar to Conway's Game of Life.
local function make_cave_map (height, width)
	-- settings
	local wall_chance = 0.40
	local born = 5 -- how many neighbors required for a new wall to appear?
	local stay = 4 -- how many neighbors for a wall to stay alive?
	local iterations = 5

	-- create the tables
	local map = {}
	for i = 1, height do
		map[i] = {}
	end

	-- fill the cells randomly
	for row = 1, height do
		for col = 1, width do
			map[row][col] = math.random() < wall_chance and WALL or FLOOR
		end
	end

	-- do a few iterations
	for i = 1, iterations do
		map = process_life (map, born, stay)
	end

	-- convert the map into a string and return it
	local tmp = {}
	for i = 1, height do
		tmp[i] = table.concat (map[i])
	end
	return table.concat (tmp)
end

function get_map ()
	local h = 25
	local w = 60
	local map = make_cave_map (h, w)

	-- add some items
	local itemlist = {}
	for i = 1, 10 do
		local x
		local y
		repeat
			x = math.random (0, w-1)
			y = math.random (0, h-1)
		until map:sub(y*w+x, y*w+x) == FLOOR
		itemlist[#itemlist+1] = {item = get_random_item(), ["x"] = x, ["y"] = y}
	end
	return {
		height = h,
		width  = w,
		tiles  = map,
		items  = itemlist,
	}
end
