--[[
combat.lua

This file defines how combat works in RSRL.
Most importantly, the function "attack" is called every time one Being
attacks another.
--]]

function attack (attacker, defender)
	local att_equip = rs.get_equipment (attacker)
	local def_equip = rs.get_equipment (defender)
	local att_equip_stats = calculate_equipment_stats (att_equip)
	local def_equip_stats = calculate_equipment_stats (def_equip)

	-- damage calculation (TODO: better calculation...)
	local damage = att_equip_stats.melee_str or 1
	if damage < 1 then damage = 1 end
	rs.adjust_hp (defender, -damage)
	rs.add_message ("defender has "..tostring(rs.get_hp(defender)).." hp left")
end
