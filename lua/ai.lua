--[[
-- ai.lua
-- Monster AIs.
--]]

-- For now just a basic AI.
-- TODO: better AI.
function do_mon_ai (mon)
	local x, y = rs.get_coords (mon)
	local player_id = rs.get_player()

	-- get Beings that are next to mon
	local nearby_beings = rs.get_in_radius (x, y, 1)

	-- move towards player
	local px, py = rs.get_coords (player_id)
	local dx, dy = rs.get_next_move (x, y, px, py)
	if dx and dy then
		-- don't attack other monsters
		local mon_on_tile = rs.get_being_on_tile (x + dx, y + dy)
		if mon_on_tile and mon_on_tile ~= player_id then
			return
		else
			rs.move (mon, dx, dy)
		end
	end
end
