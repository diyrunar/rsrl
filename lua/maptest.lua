-- A very simple piece of code for testing the map generator.

math.randomseed(os.time())
dofile ("lua/mapgen.lua")
dofile ("lua/item.lua")
local map = get_map()

for y = 0, map.height - 1 do
	for x = 1, map.width do
		local pos = y*map.width + x
		io.write (map.tiles:sub(pos, pos))
	end
	io.write("\n")
end
