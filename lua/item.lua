-- item.lua
-- Item definitions and a few helper functions.

STAT_TYPES = {
	"melee_acc",
	"melee_str",
	"melee_def",
	"range_acc",
	"range_str",
	"range_def",
	"magic_acc",
	"magic_str",
	"magic_def",
}

items = {
	cabbage = {
		name = "cabbage",
		symbol = '%',
		on_eat = function (user)
			local begin_hp = rs.get_hp (user)
			rs.adjust_hp (user, 1)
			local msg = "You eat the cabbage. Yuck!"
			if rs.get_hp(user) > begin_hp then
				msg = msg.." It heals some HP anyway."
			end
			rs.add_message (msg)
		end
	},
	bronze_dagger = {
		name = "bronze dagger",
		symbol = ')',
		stats = {
			melee_acc = 3,
			melee_str = 3,
		},
	},
}

-- Functions for using the items

function get_item_name (item)
	if items[item] then
		local name = items[item].name
		if name then
			return name
		else
			return "nameless item"
		end
	else
		return "null"
	end
end

function get_item_symbol (item)
	if items[item] then
		local sym = items[item].symbol
		if sym then return sym end
	end
end

function get_item_stats (item)
	if items[item] then
		local stats = items[item].stats
		if stats then return stats end
	end
end

-- equipment is a table consisting of {type, amount} pairs.
-- The amount is most likely not necessary, however.
function calculate_equipment_stats (equipment)
	local totals = {}
	for _, item in pairs(equipment) do
		local item_stats = get_item_stats (item["type"])
		for _, stat in pairs(STAT_TYPES) do
			totals[stat] = (totals[stat] or 0) + (item_stats[stat] or 0)
		end
	end
	return totals
end

function eat_item (user, item)
	if not items[item] then
		rs.add_message ("That item doesn't exist!")
	elseif (not items[item].on_eat) or (type(items[item].on_eat) ~= "function") then
		rs.add_message ("You can't eat that item.")
	else
		items[item].on_eat (user)
		-- TODO: remove item.
	end
end

-- how many kinds of items exist?
function count_items ()
	local count = 0
	for _ in pairs(items) do
		count = count + 1
	end
	return count
end

-- this is mostly for testing...
function get_random_item ()
	local num_items = count_items()
	local rand_item = math.random(1, num_items)
	for item in pairs(items) do
		rand_item = rand_item - 1
		if rand_item == 0 then
			return item
		end
	end
	return nil -- no items exist?
end
