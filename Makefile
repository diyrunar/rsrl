OPTS = -Wall -Wextra -Werror -Wfloat-equal -Wshadow -std=c++11
LIBS = -lsfml-graphics -lsfml-window -lsfml-system -llua
COMPILER = clang++

debug: OPTS += -O0 -g -DDEBUG
release: OPTS += -O2 -DNDEBUG

util.o: src/util.cc src/util.h
	${COMPILER} ${OPTS} -c src/util.cc
lua_util.o: src/lua_util.cc src/lua_util.h
	${COMPILER} ${OPTS} -c src/lua_util.cc
lua_api.o: src/lua_api.cc src/lua_api.h
	${COMPILER} ${OPTS} -c src/lua_api.cc
map.o: src/map.cc src/map.h
	${COMPILER} ${OPTS} -c src/map.cc
graphicssystem.o: src/graphicssystem.cc src/graphicssystem.h
	${COMPILER} ${OPTS} -c src/graphicssystem.cc
game.o: src/game.cc src/game.h
	${COMPILER} ${OPTS} -c src/game.cc
being.o: src/being.cc src/being.h
	${COMPILER} ${OPTS} -c src/being.cc
control.o: src/control.cc src/control.h
	${COMPILER} ${OPTS} -c src/control.cc
exception.o: src/exception.cc src/exception.h
	${COMPILER} ${OPTS} -c src/exception.cc
rsrl: src/main.cc game.o map.o lua_api.o util.o graphicssystem.o being.o lua_util.o control.o exception.o
	$(COMPILER) ${OPTS} -o rsrl src/main.cc game.o map.o graphicssystem.o lua_api.o util.o being.o lua_util.o control.o exception.o ${LIBS}
debug: rsrl
release: rsrl
clean:
	rm *.o rsrl
